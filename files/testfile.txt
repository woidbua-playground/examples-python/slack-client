Basic Usage

The Slack Web API allows you to build applications that interact with Slack in more complex ways than the integrations we provide out of the box.

This package is a modular wrapper designed to make Slack Web API calls simpler and easier for your app. Provided below are examples of how to interact with commonly used API endpoints, but this is by no means a complete list. Review the full list of available methods here.

See Tokens & Authentication for API token handling best practices.
