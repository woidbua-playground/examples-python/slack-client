import os
import json
import slackclient


class SlackClient():

    def __init__(self):
        slack_token = os.getenv("SLACK_TOKEN_MASTER")
        if not slack_token:
            raise KeyError("Sorry, no environment variable set for the slack bot!")
        self.client = slackclient.SlackClient(token=slack_token)

    def send_message(self, message: str, channel: str = "general"):
        self.client.api_call(
            method="chat.postMessage",
            channel=channel,
            text=message,
            as_user=True,
        )

    def send_error_message(self, title: str, message: str, channel: str = "general"):
        error_message = "** *{}* **\n".format(title)
        error_message += "```{}```".format(message)
        self.send_message(message=error_message, channel=channel)

    def upload_file(self, path: str, channel: str = "general"):
        with open(path, "rb") as in_file:
            self.client.api_call(
                method="files.upload",
                channels=channel,
                file=in_file,
                title=os.path.basename(path),
            )
