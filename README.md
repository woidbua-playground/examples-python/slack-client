# Slack Client

## References

-   [Slack API - Home](https://api.slack.com/)
-   [Slack API - Methods](https://api.slack.com/methods)

## Setup Slack Bot

-   Search & install the App `Bots`
-   Copy the API Token and create a new environemnt variable
    -   e.g. `TOKEN_SLACK_BOT_MASTER`
