import os
import traceback
from slack import SlackClient

dir_path = os.path.dirname(os.path.abspath(__file__))

client = SlackClient()

# ==============
# Example 1
# ==============
client.send_message("Hello, this is my first Bot message :)")

# ==============
# Example 2
# ==============
try:
    os.environ["somerandomstring"]
except KeyError as error:
    client.send_error_message(title=error.__class__.__name__, message=traceback.format_exc())

# ==============
# Example 3
# ==============
client.upload_file(path=os.path.join(dir_path, "README.md"))
client.upload_file(path=os.path.join(dir_path, "files", "testfile.json"))
client.upload_file(path=os.path.join(dir_path, "files", "testfile.txt"))
client.upload_file(path=os.path.join(dir_path, "files", "python-bot.png"))
